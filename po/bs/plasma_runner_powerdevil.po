# Translation of plasma_runner_powerdevil.po into Bosnian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2009, 2010.
# Dalibor Djuric <dalibor.djuric@mozilla-srbija.org>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-06 01:40+0000\n"
"PO-Revision-Date: 2012-09-03 15:49+0000\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2014-10-21 06:51+0000\n"
"X-Generator: Launchpad (build 17203)\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: PowerDevilRunner.cpp:29
#, kde-format
msgctxt "Note this is a KRunner keyword; 'power' as in 'power saving mode'"
msgid "power"
msgstr ""

#: PowerDevilRunner.cpp:30
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "suspend"
msgstr "suspenduj"

#: PowerDevilRunner.cpp:31
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to ram"
msgstr "u RAM"

#: PowerDevilRunner.cpp:32
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "sleep"
msgstr "na spavanje"

#: PowerDevilRunner.cpp:33
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hibernate"
msgstr "u hibernaciju"

#: PowerDevilRunner.cpp:34
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to disk"
msgstr "na disk"

#: PowerDevilRunner.cpp:35
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hybrid sleep"
msgstr ""

#: PowerDevilRunner.cpp:36
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hybrid"
msgstr ""

#: PowerDevilRunner.cpp:37 PowerDevilRunner.cpp:38 PowerDevilRunner.cpp:64
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "dim screen"
msgstr "priguši ekran"

#: PowerDevilRunner.cpp:48
#, kde-format
msgid ""
"Lists system suspend (e.g. sleep, hibernate) options and allows them to be "
"activated"
msgstr ""
"Nabraja opcije suspendovanja sistema (npr. spavanje, hibernacija) i "
"omogućava njihovo aktiviranje"

#: PowerDevilRunner.cpp:52
#, kde-format
msgid "Suspends the system to RAM"
msgstr "Suspenduje sistem u RAM"

#: PowerDevilRunner.cpp:56
#, kde-format
msgid "Suspends the system to disk"
msgstr "Suspenduje sistem na disk"

#: PowerDevilRunner.cpp:60
#, kde-format
msgid "Sleeps now and falls back to hibernate"
msgstr ""

#: PowerDevilRunner.cpp:63
#, fuzzy, kde-format
#| msgctxt "Note this is a KRunner keyword"
#| msgid "screen brightness"
msgctxt ""
"Note this is a KRunner keyword, <> is a placeholder and should be at the end"
msgid "screen brightness <percent value>"
msgstr "osvjetljaj ekrana"

#: PowerDevilRunner.cpp:66
#, fuzzy, no-c-format, kde-format
#| msgid ""
#| "Lists screen brightness options or sets it to the brightness defined by :"
#| "q:; e.g. screen brightness 50 would dim the screen to 50% maximum "
#| "brightness"
msgid ""
"Lists screen brightness options or sets it to the brightness defined by the "
"search term; e.g. screen brightness 50 would dim the screen to 50% maximum "
"brightness"
msgstr ""
"Nabraja opcije osvjetljaja ekrana i postavlja osvjetljaj zadat sa :q: (npr. "
"„osvjetljaj ekrana 50“ prigušuje ekran na 50% najvećeg osvjetljaja)."

#: PowerDevilRunner.cpp:83
#, fuzzy, kde-format
#| msgid "Set Brightness to %1"
msgid "Set Brightness to %1%"
msgstr "Postavi osvjetljaj na %1"

#: PowerDevilRunner.cpp:92
#, kde-format
msgid "Dim screen totally"
msgstr "Potpuno priguši ekran"

#: PowerDevilRunner.cpp:100
#, kde-format
msgid "Dim screen by half"
msgstr "Napola priguši ekran"

#: PowerDevilRunner.cpp:129
#, fuzzy, kde-format
#| msgctxt "Note this is a KRunner keyword"
#| msgid "sleep"
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "na spavanje"

#: PowerDevilRunner.cpp:130
#, kde-format
msgid "Suspend to RAM"
msgstr "Suspenduj u RAM"

#: PowerDevilRunner.cpp:135
#, fuzzy, kde-format
#| msgctxt "Note this is a KRunner keyword"
#| msgid "hibernate"
msgctxt "Suspend to disk"
msgid "Hibernate"
msgstr "u hibernaciju"

#: PowerDevilRunner.cpp:136
#, fuzzy, kde-format
#| msgid "Suspend to Disk"
msgid "Suspend to disk"
msgstr "Suspenduj na disk"

#: PowerDevilRunner.cpp:141
#, kde-format
msgctxt "Suspend to both RAM and disk"
msgid "Hybrid sleep"
msgstr ""

#: PowerDevilRunner.cpp:142
#, kde-format
msgid "Sleep now and fall back to hibernate"
msgstr ""

#: PowerDevilRunner.cpp:222
#, fuzzy, kde-format
#| msgctxt "Note this is a KRunner keyword"
#| msgid "screen brightness"
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "screen brightness "
msgstr "osvjetljaj ekrana"

#: PowerDevilRunner.cpp:224
#, fuzzy, kde-format
#| msgctxt "Note this is a KRunner keyword"
#| msgid "dim screen"
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "dim screen "
msgstr "priguši ekran"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "screen brightness"
#~ msgstr "osvjetljaj ekrana"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "screen brightness %1"
#~ msgstr "osvjetljaj ekrana %1"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "dim screen %1"
#~ msgstr "priguši ekran %1"

#, fuzzy
#~| msgctxt "Note this is a KRunner keyword"
#~| msgid "suspend"
#~ msgid "Suspend"
#~ msgstr "suspenduj"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "power profile"
#~ msgstr "profil napajanja"

#~ msgid "Lists all power profiles and allows them to be activated"
#~ msgstr "Nabraja sve profile napajanja i omogućava njihovo aktiviranje"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "power profile %1"
#~ msgstr "profil napajanja %1"

#~ msgid "Set Profile to '%1'"
#~ msgstr "Postavi profil „%1“"

#~ msgid "Turn off screen"
#~ msgstr "Ugasi ekran"
