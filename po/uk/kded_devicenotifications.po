# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-19 01:58+0000\n"
"PO-Revision-Date: 2023-07-22 18:10+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: devicenotifications.cpp:275
#, kde-format
msgid "%1 has been plugged in."
msgstr "%1 з'єднано."

#: devicenotifications.cpp:275
#, kde-format
msgid "A USB device has been plugged in."
msgstr "З'єднано пристрій USB."

#: devicenotifications.cpp:278
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "Виявлено пристрій USB"

#: devicenotifications.cpp:295
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 було від'єднано."

#: devicenotifications.cpp:295
#, kde-format
msgid "A USB device has been unplugged."
msgstr "Пристрій USB від'єднано."

#: devicenotifications.cpp:298
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "Вилучено пристрій USB"
