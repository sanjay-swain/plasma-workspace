# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin, 2014, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2023-08-07 23:19+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Chinese <zh-l10n@lists.slat.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 23.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Franklin Weng, Jeff Huang"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "franklin@goodhorse.idv.tw, s8321414@gmail.com"

#: main.cpp:70 view.cpp:47
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:70
#, kde-format
msgid "Run Command interface"
msgstr "執行指令介面"

#: main.cpp:76
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "將剪貼簿內容作為 KRunner 的查詢內容使用"

#: main.cpp:77
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "背景執行 KRunner 而不顯示。"

#: main.cpp:78
#, kde-format
msgid "Replace an existing instance"
msgstr "取代現存實例"

#: main.cpp:79
#, kde-format
msgid "Show only results from the given plugin"
msgstr "只顯示所選外掛程式提供的結果"

#: main.cpp:80
#, kde-format
msgid "List available plugins"
msgstr "列出可用的外掛程式"

#: main.cpp:87
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "要執行的查詢內容，只能在未提供 -c 時使用"

#: main.cpp:96
#, kde-format
msgctxt "Header for command line output"
msgid "Available KRunner plugins, pluginId"
msgstr "可用的 KRunner 外掛程式、外掛程式 ID"

#: qml/RunCommand.qml:94
#, kde-format
msgid "Configure"
msgstr "設定"

#: qml/RunCommand.qml:95
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "設定 KRunner 行為"

#: qml/RunCommand.qml:98
#, kde-format
msgid "Configure KRunner…"
msgstr "設定 KRunner…"

#: qml/RunCommand.qml:111
#, kde-format
msgid "Showing only results from %1"
msgstr "目前僅顯示來自 %1 的結果"

#: qml/RunCommand.qml:125
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "搜尋「%1」…"

#: qml/RunCommand.qml:126
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "搜尋…"

#: qml/RunCommand.qml:297 qml/RunCommand.qml:298 qml/RunCommand.qml:300
#, kde-format
msgid "Show Usage Help"
msgstr "顯示用法說明"

#: qml/RunCommand.qml:308
#, kde-format
msgid "Pin"
msgstr "釘選"

#: qml/RunCommand.qml:309
#, kde-format
msgid "Pin Search"
msgstr "釘選搜尋"

#: qml/RunCommand.qml:311
#, kde-format
msgid "Keep Open"
msgstr "保持開啟"

#: qml/RunCommand.qml:383 qml/RunCommand.qml:388
#, kde-format
msgid "Recent Queries"
msgstr "最近查詢"

#: qml/RunCommand.qml:386
#, kde-format
msgid "Remove"
msgstr "移除"

#~ msgid "krunner"
#~ msgstr "krunner"
